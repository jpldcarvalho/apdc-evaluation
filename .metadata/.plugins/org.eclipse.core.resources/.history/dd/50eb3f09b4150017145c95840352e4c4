package pt.unl.fct.di.apdc.evaluation.resources;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.codec.digest.DigestUtils;

import javax.servlet.http.*;

import com.google.gson.*;
import com.google.appengine.api.datastore.*;

import java.util.*;
import java.util.logging.*;
import pt.unl.fct.di.apdc.evaluation.util.*;

@Path("/login")
public class LoginResource {

	private static final Logger LOG = Logger.getLogger(LoginResource.class.getName());
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	private final Gson g = new Gson();
	
	public LoginResource() { }
	
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response login(LoginData data, @Context HttpServletRequest request,
						  @Context HttpHeaders headers) {
		LOG.fine("Attempt to login user: " + data.username);
		
		Transaction txn = datastore.beginTransaction();
		Key userkey = KeyFactory.createKey("User", data.username);
		
		try {
			Entity user = datastore.get(userkey);
			
			//Login statistics
			Query ctrQuery = new Query("UserStats").setAncestor(userkey);
			List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());
			Entity stats;
			if(results.isEmpty()) {
				stats = new Entity("UserStats", user.getKey());
				stats.setProperty("user_stats_logins", 0L);
				stats.setProperty("user_stats_failed", 0L);
			} else {
				stats = results.get(0);
			}
			
			String hashedPassword = (String) user.getProperty("user_password");
			if(hashedPassword.equals(DigestUtils.sha512Hex(data.password))) {
				//Logs
				Entity log = new Entity("UserLog", user.getKey());
				log.setProperty("user_login_ip", request.getRemoteAddr());
				log.setProperty("user_login_host", request.getRemoteHost());
				log.setProperty("user_login_latlon", headers.getHeaderString("X-AppEngine-CityLatLong"));
				log.setProperty("user_login_city", headers.getHeaderString("X-AppEngine-City"));
				log.setProperty("user_login_country", headers.getHeaderString("X-AppEngine-Country"));
				log.setProperty("user_login_time", new Date());
				
				//Token
				AuthToken tok = new AuthToken(data.username);
				Entity token = new Entity("UserToken", user.getKey());
				token.setProperty("user_token_id", tok.tokenID);
				token.setProperty("user_token_username", tok.username);
				token.setProperty("user_token_creation_data", tok.creationData);
				token.setProperty("user_token_expiration_data", tok.expirationData);
				
				//Get user statistics and updates it
				stats.setProperty("user_stats_logins", 1L + (long) stats.getProperty("user_stats_logins"));
				stats.setProperty("user_stats_failed", 0L);
				stats.setProperty("user_stats_last", new Date());
				
				//Batch operation
				List<Entity> logs = Arrays.asList(log, stats);
				datastore.put(txn, logs);
				txn.commit();
				
				LOG.info("User '" + data.username + "' logged in sucessfully.");
				return Response.ok(g.toJson(token)).build();
			} else {
				stats.setProperty("user_failed", 1L + (long) stats.getProperty("user_stats_failed"));
				datastore.put(txn, stats);
				txn.commit();
				
				LOG.warning("Wrong password for user: " + data.username);
				return Response.status(Status.FORBIDDEN).build();
			}
			
		} catch (EntityNotFoundException e) {
			LOG.warning("Failed login attempt for user: " + data.username);
			return Response.status(Status.FORBIDDEN).build();
		} finally {
			if(txn.isActive())
				txn.rollback();
		}
	}
}
