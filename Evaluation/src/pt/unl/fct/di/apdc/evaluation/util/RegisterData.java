package pt.unl.fct.di.apdc.evaluation.util;

public class RegisterData {

	public String username;
	public String name;
	public String email;
	public int phoneNumber;
	public int mobileNumber;
	public String street;
	public String city;
	public String zipCode;
	public int nif;
	public int cc;
	public String password;
	
	public RegisterData() { }
	
	public RegisterData(String username, String name, String email, int phoneNumber,
			int mobileNumber, String street, String city, String zipCode, int nif,
			int cc, String password) {
		this.username = username;
		this.name = name;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.mobileNumber = mobileNumber;
		this.street = street;
		this.city = city;
		this.zipCode = zipCode;
		this.nif = nif;
		this.cc = cc;
		this.password = password;
	}
	
	public boolean validate() {
		return true;
	}
}
