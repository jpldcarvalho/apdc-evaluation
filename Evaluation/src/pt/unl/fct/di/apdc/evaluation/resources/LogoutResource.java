package pt.unl.fct.di.apdc.evaluation.resources;

import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import javax.ws.rs.core.Response.Status;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.gson.Gson;

import pt.unl.fct.di.apdc.evaluation.util.LogoutData;

@Path("/logout")
public class LogoutResource {

	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response logout(LogoutData data) {
		try {
			Key userkey = KeyFactory.createKey("User", data.username);
			
			Entity user = datastore.get(userkey);

			Query ctrQuery = new Query("UserToken").setAncestor(userkey);
			List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());
			Entity log = results.get(0);
			
			String token = (String) log.getProperty("user_token_id");
			
			if(!token.equals(data.token))
				return Response.status(Status.UNAUTHORIZED).entity("Token ID don't correspond.").build();
			
			log.setProperty("user_token_id", 0);
			log.setProperty("user_token_creation_data", 0);
			log.setProperty("user_token_expiration_data", 0);
			
			return Response.ok().build();
		} catch (EntityNotFoundException e) {
			return Response.status(Status.BAD_REQUEST).entity("User not valid.").build();
		}
	}
}
