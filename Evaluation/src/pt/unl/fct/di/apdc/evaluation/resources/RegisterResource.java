package pt.unl.fct.di.apdc.evaluation.resources;

import java.util.Date;
import java.util.logging.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.codec.digest.DigestUtils;

import com.google.appengine.api.datastore.*;

import pt.unl.fct.di.apdc.evaluation.util.*;

@Path("/register")
public class RegisterResource {
	
	private static final Logger LOG = Logger.getLogger(LoginResource.class.getName());
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	
	public RegisterResource() { }
	
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response regist(RegisterData data) {
		
		if(! data.validate()) {
			return Response.status(Status.BAD_REQUEST).entity("Missing or wrong parameter.").build();
		}
		
		Transaction txn = datastore.beginTransaction();
		try {
			Key userkey = KeyFactory.createKey("User", data.username);
			@SuppressWarnings("unused")
			Entity user = datastore.get(userkey);
			txn.rollback();
			return Response.status(Status.BAD_REQUEST).entity("User already exists.").build();
		} catch (EntityNotFoundException e) {
			Entity user = new Entity("User", data.username);
			user.setIndexedProperty("user_username", data.username);
			user.setIndexedProperty("user_name", data.name);
			user.setIndexedProperty("user_email", data.email);
			user.setIndexedProperty("user_phone_number", data.phoneNumber);
			user.setIndexedProperty("user_mobile_number", data.mobileNumber);
			user.setIndexedProperty("user_street", data.street);
			user.setIndexedProperty("user_city", data.city);
			user.setIndexedProperty("user_zipCode", data.zipCode);
			user.setIndexedProperty("user_nif", data.nif);
			user.setIndexedProperty("user_cc", data.cc);
			user.setIndexedProperty("user_password", DigestUtils.sha512Hex(data.password));
			user.setUnindexedProperty("user_creation_time", new Date());
			
			datastore.put(txn, user);
			LOG.info("User registered " + data.username);
			txn.commit();
			return Response.ok().build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
		}
	}
}
