package pt.unl.fct.di.apdc.evaluation.util;

public class SearchUserData {

	public String username;
	public String name;
	public String token;
	
	public SearchUserData() { }
	
	public SearchUserData(String username, String name, String token) {
		this.username = username;
		this.name = name;
		this.token = token;
	}
}
