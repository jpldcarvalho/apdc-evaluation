app.controller('LoginController', function($scope, $http) {

    $scope.user = {};

    $scope.submitForm = function () {
        $http({
            method  : 'POST',
            url     : '/api/login',
            data    : $scope.user,
            headers : {"Content-Type": "application/json; charset=utf-8 "}
        }).success(function (data) {
        	localStorage.clear();
        	localStorage.setItem('username', $scope.user.username);
            localStorage.setItem('tokenID', data.propertyMap.user_token_id);
            $scope.log();
            window.location = "#home";
        }).error(function (err) {
        	window.location = "#login-error";
        })
    }

});