app.controller('MapController', function($scope, $http) {

    $scope.user = {
    		"username":localStorage.getItem('username'),
        	"token":localStorage.getItem('tokenID')
    };
    
    $scope.submitForm = function () {
        $http({
            method  : 'POST',
            url     : '/api/location',
            data    : $scope.user,
            headers : {"Content-Type": "application/json; charset=utf-8 "}
        }).success(function (data) {
        	console.log(data);
        }).error(function (err) {
        	
        })
    }

});