app.controller('MainController', function($scope, $http) {
	$scope.left = [
    	{"name":"Home","url":"#home","show":true},
    	{"name":"Search user","url":"#user-map","show":false},
    	{"name":"Search all users","url":"#all-users-map","show":false}
    ];

    $scope.right = [
    	{"name":"Register","url":"#register","show":true},
    	{"name":"Login","url":"#login","show":true},
    	{"name":"Logout","url":"#home","show":false}
    ];
    
    $scope.log = function() {
    	$scope.left[1].show = true;
        $scope.left[2].show = true;
        $scope.right[0].show = false;
        $scope.right[1].show = false;
        $scope.right[2].show = true;
    }
    
    $scope.logout = function() {
    	console.log(localStorage.getItem('username')+localStorage.getItem('tokenID'));
    	$http({
            method  : 'POST',
            url     : '/api/logout',
            data    : {
            	"username":localStorage.getItem('username'),
            	"token":localStorage.getItem('tokenID')
            },
            headers : {"Content-Type": "application/json; charset=utf-8 "}
        }).success(function (data) {
        	$scope.left[1].show = false;
            $scope.left[2].show = false;
            $scope.right[0].show = true;
            $scope.right[1].show = true;
            $scope.right[2].show = false;
            localStorage.removeItem('tokenID');
        }).error(function (err) {
        	window.location = "#logout-error";
        })
        
    	
    }
});