var app = angular.module("myApp", ['ngRoute']);

app.run(function($rootScope) {
	
});

app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : "home.html"
    })
    .when("/home", {
        templateUrl : "home.html"
    })
    .when("/user-map", {
        templateUrl : "user-map.html"
    })
    .when("/all-users-map", {
        templateUrl : "all-users-map.html"
    })
    .when("/login", {
        templateUrl : "login.html"
    })
    .when("/login-error", {
        templateUrl : "login-error.html"
    })
    .when("/register", {
        templateUrl : "register.html"
    })
    .when("/register-success", {
        templateUrl : "register-success.html"
    })
    .when("/register-error", {
        templateUrl : "register-error.html"
    })
    .when("/logout", {
        templateUrl : "logout.html"
    });
});