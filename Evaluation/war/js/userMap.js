var geocoder;
var map;
var address ="Rua Dr. Francisco Costa nº 3, Abela Santiago do Cacém 7540-011";

function initMap() {
	/*map = new google.maps.Map(document.getElementById('map'),
		{
			center: {lat: 38.659784, lng: -9.202765},
			zoom: 16
		}
	);
	
	var infowindow = new google.maps.InfoWindow(
            { content: '<b>'+address+'</b>',
              size: new google.maps.Size(150,50)
            });

	
	var sala116 = new google.maps.LatLng(38.66104, -9.2032);
	
	var marker = new google.maps.Marker({
		position: sala116,
		map: map
	});*/
	
	    geocoder = new google.maps.Geocoder();
	    var latlng = new google.maps.LatLng(-34.397, 150.644);
	    var myOptions = {
	      zoom: 8,
	      center: latlng,
	    mapTypeControl: true,
	    mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
	    navigationControl: true,
	      mapTypeId: google.maps.MapTypeId.ROADMAP
	    };
	    map = new google.maps.Map(document.getElementById("map"), myOptions);
	    if (geocoder) {
	      geocoder.geocode( { 'address': address}, function(results, status) {
	        if (status == google.maps.GeocoderStatus.OK) {
	          if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
	          map.setCenter(results[0].geometry.location);

	            var infowindow = new google.maps.InfoWindow(
	                { content: '<b>'+address+'</b>',
	                  size: new google.maps.Size(150,50)
	                });
	    
	            var marker = new google.maps.Marker({
	                position: results[0].geometry.location,
	                map: map, 
	                title:address
	            }); 
	            google.maps.event.addListener(marker, 'click', function() {
	                infowindow.open(map,marker);
	            });

	          } else {
	            alert("No results found");
	          }
	        } else {
	          alert("Geocode was not successful for the following reason: " + status);
	        }
	      });
	    }
}